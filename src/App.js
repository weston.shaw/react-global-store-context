import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import ToolBar from "./components/elements/ToolBar";
import "./App.css";

import Home from "./components/pages/Home";
import Test from "./components/pages/Test";
import { StoreProvider } from "./store/store";

function App() {
  return (
    <div className="App">
      <StoreProvider>
        <Router>
          <ToolBar></ToolBar>
          <Route path="/" exact component={Home} />
          <Route path="/test" exact component={Test} />
        </Router>
      </StoreProvider>
    </div>
  );
}

export default App;
