import React from "react";
import { useStore } from "../../store/store";
import { useBooks } from "../../hooks/books";

const Home = () => {
  const { store } = useStore();
  const { addBook } = useBooks();

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();
          const data = Array.from(e.target.elements).reduce((a, c) => {
            if (!c.name) {
              return a;
            }
            a[c.name] = c.value;
            return a;
          }, {});
          addBook(data);
        }}
      >
        <p>
          <input placeholder="title" name="title" type="text" />
        </p>

        <p>
          <input placeholder="Author" name="author" type="text" />
        </p>
        <button type="submit">Add Book</button>
      </form>
      <pre>{JSON.stringify(store, false, 2)}</pre>
    </div>
  );
};
export default Home;
