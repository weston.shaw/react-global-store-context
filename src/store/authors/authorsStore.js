const consts = ["ADD_AUTHOR"];

const authorsConst = consts.reduce((a, c) => {
  a[c] = c;
  return a;
}, {});

const authorsReducer = (authors = [], action) => {
  const { type, payload } = action;
  switch (type) {
    case authorsConst.ADD_AUTHOR:
      return [...authors, payload.author];
    default:
      return authors;
  }
};

export { authorsReducer, authorsConst };
