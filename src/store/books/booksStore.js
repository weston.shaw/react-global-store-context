const consts = ["ADD_BOOK"];

const booksConst = consts.reduce((a, c) => {
  a[c] = c;
  return a;
}, {});

const booksReducer = (books = [], action) => {
  const { type, payload } = action;
  switch (type) {
    case booksConst.ADD_BOOK:
      return [...books, payload.book];
    default:
      return books;
  }
};

export { booksReducer, booksConst };
