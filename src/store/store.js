import React, { createContext, useContext, useReducer } from "react";
import { reducers, initialState } from "./reducers";

export const StoreContext = createContext();
export const useStore = () => useContext(StoreContext);

export const StoreProvider = ({ children }) => {
  const [store, dispatch] = useReducer(reducers, initialState);
  return (
    <StoreContext.Provider value={{ store, dispatch }}>
      {children}
    </StoreContext.Provider>
  );
};
