import combineReducers from "./combineReducers";

import { authorsReducer } from "./authors/authorsStore";
import { booksReducer } from "./books/booksStore";

const [reducers, initialState] = combineReducers({
  authors: authorsReducer,
  books: booksReducer
});

export { reducers, initialState };
