export default function combineReducers(reducers) {
  const devtools = window && window.__REDUX_DEVTOOLS_EXTENSION__;
  if (devtools) {
    devtools.connect();
  }
  const reduce = (state, action) => {
    const newState = Object.keys(reducers).reduce((a, c) => {
      a[c] = reducers[c](state[c], action);
      return a;
    }, {});
    if (devtools) {
      devtools.send({ ...action }, newState);
    }
    return newState;
  };
  return [reduce, reduce({}, { type: "@@INIT" })];
}
