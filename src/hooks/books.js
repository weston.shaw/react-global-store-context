import { useStore } from "../store/store";
import { booksConst } from "../store/books/booksStore";

export const useBooks = () => {
  const { store, dispatch } = useStore();
  const { books } = store;

  const updateBook = book => {};

  const removeBook = book => {};

  const addBook = book => {
    dispatch({ type: booksConst.ADD_BOOK, payload: { book } });
  };

  return {
    updateBook,
    removeBook,
    addBook,
    books
  };
};
