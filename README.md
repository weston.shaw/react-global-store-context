# React Global Context Store

The idea is to use hooks to mimic the way Redux controls global app state. Rather than having actions and middleware control business logic, use custom hooks to do validation and call dispatch actions.
